import {  } from 'lodash';
import { actionTypes } from './action-types';
import { User } from '../types/User';
import { Message } from '../types/Message';

const makeActionHandler = <T>(actionType: actionTypes) => {
  return (data: T) => ({
    type: actionType,
    payload: data
  }) 
}

export const setUsers = makeActionHandler<User[]>(actionTypes.setUsers);
export const setCurrentUser = makeActionHandler<User>(actionTypes.setCurrentUser);
export const setMessages = makeActionHandler<Message[]>(actionTypes.setMessages);
export const setEditedMessage = makeActionHandler<Message | null>(actionTypes.setEditedMessage);

