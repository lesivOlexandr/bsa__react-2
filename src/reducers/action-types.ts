export enum actionTypes {
  setUsers = 'SET_USERS',
  setCurrentUser = 'SET_CURRENT_USER',
  setMessages = 'SET_MESSAGES',
  setEditedMessage = 'SET_EDITED_MESSAGE'
}
