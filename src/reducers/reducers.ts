import { actionTypes } from './action-types';
import { AppState } from '../types/app-state';

const initialState: AppState = new AppState();

type action = {type: actionTypes, payload: any};

export default (state: AppState = initialState, action: action): AppState => {
  switch(action.type) {
    case actionTypes.setUsers:
      return {
        ...state,
        users: [...action.payload]
      }
    case actionTypes.setCurrentUser:
      return {
        ...state,
        user: action.payload
      }
    case actionTypes.setMessages:
      return {
        ...state,
        messages: [...action.payload]
      }
    case actionTypes.setEditedMessage:
      return {
        ...state,
        editedMessage: action.payload ? { ...action.payload }: null
      }
    default:
      return state
  }
}