import { User } from "./User";
import { Message } from "./Message";

export class AppState {
  constructor(
    public users: User[] = [],
    public user: User | null = null,
    public messages: Message[] = [],
    public editedMessage: Message | null = null 
  ){}
}
