export abstract class Observable<T> {
  private observers: Set<Observer<T>> = new Set();

  public subscribe(observer: Observer<T>) {
    if (this.observers.size === 0) {
      this.onFirstSubscription();
    }
    this.observers.add(observer);
  }

  public unsubscribe(observer: Observer<T>) {
    this.observers.delete(observer);
    if (this.observers.size === 0) {
      this.onNoSubscriptions();
    }
  }

  public notifyObservers(data: T): void {
    this.observers.forEach(observer => observer(data));
  }

  protected onFirstSubscription(): void {

  }

  protected onNoSubscriptions(): void {

  }
}

export interface Observer<T> {
  (data: T): void
}
