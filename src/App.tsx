import React, { useState } from 'react';
import { connect } from 'react-redux';
import ChatComponent from './components/chat'; 
import LoginComponent from './components/login';
import AppHeader from './components/app-header';
import AppFooter from './components/app-footer/';
import Preloader from './components/preloader';
import { User } from './types/User';
import { Message } from './types/Message';
import { messageService } from './services/message.service';
import { userService } from './services/users.service';
import { setUsers, setCurrentUser, setMessages } from './reducers/actions';
import { AppState } from './types/app-state';

import './App.css';

const App = ({
  user, 
  setUsers,
  setCurrentUser,
  setMessages
}: {
  user: User | null,
  setUsers: (users: User[]) => void,
  setCurrentUser: (user: User) => void,
  setMessages: (message: Message[]) => void
}) => {
  const [isLoading, setIsLoading] = useState<boolean>(false);

  const onAuthorized = (user: User) => {
    setCurrentUser(user);
    setIsLoading(true);
    messageService.subscribe((messages: Message[]) => { setMessages(messages); setIsLoading(false) });
    userService.subscribe((users: User[]) => setUsers(users));
  };

  const getContent = () => {
    return isLoading ? <Preloader /> : <ChatComponent />;
  };

  return (
    <div className="app">
      <AppHeader />
        {user 
          ? getContent()
          : <LoginComponent onAuthorized={onAuthorized} />
        }
      <AppFooter />
    </div>
  );
}

const mapStateToProps = (state: AppState) => {
  return {
    user: state.user,
  }
}

const mapDispatchToProps = {
  setUsers, setCurrentUser, setMessages
}

export default connect(mapStateToProps, mapDispatchToProps)(App);
