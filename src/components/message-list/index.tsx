import React from 'react';
import propTypes from 'prop-types';
import moment from 'moment';
import { connect } from 'react-redux';
import { Message } from '../../types/Message';
import { User } from '../../types/User';
import MessageComponent from '../message/message';
import { day } from '../../helpers/date-helpers';
import { AppState } from '../../types/app-state';

import './index.css';

const MessageList = ({messages, user}: {messages: Message[], user: User}) => {
  const dateMessages: {[key: string]: Message[]} = {};

  for (const message of messages) {
    const date: Date = new Date(message.createdAt);
    let key: string = '';

    if (Date.now() - date.valueOf() < day) {
      key = moment(date).fromNow();
    } else {
      key = moment(date).format('DD/MM/YYYY');
    }

    if (!dateMessages[key]) {
      dateMessages[key] = [];
    }

    dateMessages[key].push(message);
  }

  const dates: string[] = Object.keys(dateMessages);

  return (
    <div className="chat__chat-body"> 
      {dates.map(key => (
        <div key={key} className="chat-body__group">
          <div className="chat-body__divided">
            <div className="chat-body__date">{key}</div>
          </div>
          {dateMessages[key].map(message => <MessageComponent user={user} message={message} key={message.id} />)}
        </div>
        ))}
    </div>
  );
}

(MessageList as any).propTypes = {
  messages: propTypes.arrayOf(propTypes.object).isRequired,
  user: propTypes.object.isRequired
}

const mapStateToProps = (state: AppState) => {
  return {
    messages: state.messages,
    user: state.user!
  }
}

export default connect(mapStateToProps)(MessageList);