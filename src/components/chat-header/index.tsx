import React, { useState } from 'react';
import { connect } from 'react-redux';
import propTypes from 'prop-types';
import { Message } from '../../types/Message';
import { User } from '../../types/User';
import { formatDate } from '../../helpers/date-helpers';
import { AppState } from '../../types/app-state';

import './index.css';

const ChatHeader = (
  {messages, users}: 
  {messages: Message[], users: User[]}
) => {
  const [showUsers, setShowUsers] = useState(false);

  const usersCount: number = users.length;
  const messagesCount: number = messages.length;
  let lastMessageSendTime: Date | null = null;
  for (const message of messages) {
    const messageDate: Date = new Date(message.createdAt);
    if (lastMessageSendTime === null || messageDate > lastMessageSendTime) {
      lastMessageSendTime = messageDate;
    }
  }

  const onShowUsers = () => setShowUsers(!showUsers);

  return (
    <div className="chat__chat-header">
      <div className="chat-header__general-inf">
        <h2 className="chat-header__name chat-header__general-inf-item">My Chat</h2>
        <div className="chat-header__users-count chat-header__general-inf-item">
          <span
            onClick={onShowUsers} 
            className="general-inf__users-count"
          >{usersCount} Participants</span>
          {showUsers && 
            <div className="chat-header__users">
              <h3>Chat Users</h3>
              <ul className="chat-header__user-list">
                {users.map(user => (
                  <li key={user.id}>
                    <span className="chat-header__user-list-item">{ user.name }</span>
                  </li>
                ))}
              </ul>
            </div>
          }
        </div>
        <div className="chat-header__messages-count chat-header__general-inf-item">{messagesCount} messages</div>
      </div>
      <div className="char-header__date">
        { lastMessageSendTime ? 'Last message at ' + formatDate(lastMessageSendTime): '-' }
      </div>
    </div>
  )  
}

(ChatHeader as any).propTypes = {
  users: propTypes.arrayOf(propTypes.object).isRequired,
  messages: propTypes.arrayOf(propTypes.object).isRequired
}

const mapStateToProps = (state: AppState) => {
  return {
    users: state.users,
    messages: state.messages
  }
}

export default connect(mapStateToProps)(ChatHeader);
