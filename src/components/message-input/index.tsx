import React, { FormEvent, ChangeEvent, useState } from 'react';
import propTypes from 'prop-types';
import { connect } from 'react-redux';
import { messageService } from '../../services/message.service';
import { User } from '../../types/User';
import { AppState } from '../../types/app-state';

import './index.css';

const MessageInput = ({user}: {user: User}) => {
  let [message, setMessage] = useState<string>();

  const onSubmit = (e: FormEvent) => {
    e.preventDefault();
    if (message) {
      messageService.saveMessage(message, user);
      setMessage('');
    }
  }

  const onChange = (e: ChangeEvent<HTMLTextAreaElement>) => setMessage(e.target.value);

  return (
    <div className="chat__message-input-wrapper">
      <form className="chat__message-input-form" onSubmit={onSubmit}>
        <textarea className="message-input-form__message-input" placeholder="Message" value={message} onChange={onChange} />
        <button className="message-input-form__submit-button" type="submit">
          <span className="material-icons submit-button__icon">publish</span>
          Send
        </button>
      </form>
    </div>
  );
}

(MessageInput as any).propTypes = {
  user: propTypes.object.isRequired
}

const mapStateToProps = (state: AppState) => {
  return {
    user: state.user!
  }
}

export default connect(mapStateToProps)(MessageInput);
