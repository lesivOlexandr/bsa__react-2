import React, { FormEvent, ChangeEvent } from 'react';
import { setEditedMessage } from '../../reducers/actions';
import { connect } from 'react-redux';
import { Message } from '../../types/Message';
import { AppState } from '../../types/app-state';
import { messageService } from '../../services/message.service';

import './index.css';

const EditMessageModal = ({
  editedMessage, 
  setEditedMessage
}: {
  editedMessage: Message,
  setEditedMessage: (data: Message | null) => void
}) => {
  let inputtedEditText = '';
  const onCancel = (e: FormEvent) => {
    e.preventDefault();
    setEditedMessage(null);
  }

  const onSubmitEdit = (e: FormEvent) => {
    e.preventDefault();
    setEditedMessage(null);
    if (inputtedEditText) {
      editedMessage.text = inputtedEditText;
      editedMessage.editedAt = Date.now();
      messageService.updateMessage(editedMessage.id, editedMessage);
    }
  }

  const onTextChange = (e: ChangeEvent<HTMLTextAreaElement>) => {
    inputtedEditText = e.target.value;
  }

  return (
    <div className="chat__modal-backdrop">
      <div className="chat__modal-container">
        <form className="modal-container-wrapper" onReset={onCancel} onSubmit={onSubmitEdit}>
          <h3>Input new message text</h3>
          <textarea 
            className="modal-container__edit"
            placeholder="message" 
            defaultValue={editedMessage.text}
            onChange={onTextChange} 
          />
          <div className="modal-container__buttons">
            <button className="modal-container__submit-edit-button" type="submit">Save</button>
            <button className="modal-container__cancel-edit-button" type="reset">Cancel</button>
          </div>
        </form>
      </div>
    </div>
  );
}

const mapStateToProps = (state: AppState) => ({
  editedMessage: state.editedMessage!
});

const mapDispatchToProps = {
  setEditedMessage
}

export default connect(mapStateToProps, mapDispatchToProps)(EditMessageModal);
