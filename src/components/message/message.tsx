import React from 'react';
import classNames from "classnames";
import propTypes from 'prop-types';
import { Message } from '../../types/Message';
import { User } from '../../types/User';
import './index.css';
import { formatDate } from '../../helpers/date-helpers';
import { messageService } from '../../services/message.service';
import { setEditedMessage } from '../../reducers/actions';
import { connect } from 'react-redux';

const MessageComponent = (
  {
    message, 
    user, 
    setEditedMessage
  }: {
    message: Message,
    user: User,
    setEditedMessage: (message: Message | null) => void
  }
) => {
  const isMessageOfCurrentUser: boolean = message.userId === user.id;
  const isLiked: boolean = message.likedBy.includes(user.id);

  const onClickLike = () => {
    if (isMessageOfCurrentUser) {
      alert('You can\'t like your own post');
      return;
    }
    if (isLiked) {
      message.likedBy = message.likedBy.filter((userId: string) => user.id !== userId);
    }
    if (!isLiked) {
      message.likedBy.push(user.id)
    }
    messageService.updateMessage(message.id, message);
  }

  const onClickDelete = () => {
    messageService.deleteMessage(message.id);
  }

  const onClickEdit = () => {
    setEditedMessage(message);
  }

  return (
    <div className={classNames({
      'chat-body__chat-message': true,
      'chat-body__chat-message_user-current': isMessageOfCurrentUser
    })}>
      {
        !isMessageOfCurrentUser &&
        <div className="chat-message__user-avatar-wrapper">
          <img className="chat-message__user-avatar" src={message.avatar ? message.avatar: '/user.png'} alt="" />
        </div>
      }
      <div className="chat-message__text-wrapper">
        <p className="chat-message__text">
          { message.text }
        </p>
        <div className="chat-message__message-meta">
          <div className="message-meta__message-icons">
            <div className={classNames({
              "message-icons__likes-block": true,
              "message-meta__like-icon_owner-current-user": isMessageOfCurrentUser,
              "message-meta__like-icon_state-active": isLiked
            })}>
              <span className="likes-block__likes-count">
                {message.likedBy.length}
              </span>
              <span 
                onClick={onClickLike}
                className={classNames({
                  "material-icons message-meta__like-icon": true,
                  "message-icon": true
                })}>
                thumb_up
              </span>
            </div>
            {isMessageOfCurrentUser &&
              <>
                <span 
                  className="material-icons message-icon hidden-until"
                  onClick={onClickEdit}
                >
                  settings
                </span>
                <span 
                  className="material-icons message-icon hidden-until"
                  onClick={onClickDelete}
                >
                  delete
                </span>
              </>
            }
          </div>
          <div className="message-meta__time-send">
            { message.editedAt 
              ? 'Edited at ' + formatDate(new Date(message.editedAt))
              : 'Created at ' + formatDate(new Date(message.createdAt)) }
          </div>
        </div>
      </div>
    </div>
  );
}

(MessageComponent as any).propTypes = {
  message: propTypes.object.isRequired,
  user: propTypes.object.isRequired
}

const mapDispatchToProps = {
  setEditedMessage
}

export default connect(null, mapDispatchToProps)(MessageComponent);
