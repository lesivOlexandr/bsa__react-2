import { dataStorageProvider } from '../storage/firebase';
import { firestore } from 'firebase';
import { User } from '../types/User';
import { v4 as uuid } from 'uuid';

export class AuthenticationService {

  public async createUser(userName: string, password: string): Promise<User> {
    const userData: User = { id: uuid(), name: userName, password };
    const response: firestore.DocumentReference<firestore.DocumentData> = await dataStorageProvider
      .collection('users')
      .add(userData)
    const user: User = (await response.get()).data() as User;
    return user;
  }

  public async login(userName: string, password: string): Promise<User | null> {
    const response: firestore.QuerySnapshot<firestore.DocumentData> = await dataStorageProvider
      .collection('users')
      .where('name', '==', userName)
      .where('password', '==', password)
      .limit(1)
      .get()
    if (response.size === 1) {
      return response.docs[0].data() as User;
    }
    return null;
  }
}

export const authenticationService = new AuthenticationService();
