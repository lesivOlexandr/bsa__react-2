import { firestore } from 'firebase';
import { v4 as uuid } from 'uuid';

import { dataStorageProvider } from '../storage/firebase';
import { Message } from '../types/Message';
import { Observable } from '../types/Observable';
import { User } from '../types/User';

export class MessageService extends Observable<Message[]> {

  protected onFirstSubscription(): void {
    dataStorageProvider
      .collection('messages')
      .orderBy('createdAt', 'asc')
      .onSnapshot((snapshot: firestore.QuerySnapshot<firestore.DocumentData>) => {
        const messages: Message[] = [];
        snapshot.forEach(doc => messages.push(doc.data() as Message));

        this.setMessages(messages);
      });
  }

  private setMessages(messages: Message[]): void {
    this.notifyObservers(messages);
  }

  // Is not this violating single responsibility principle?
  public async saveMessage(text: string, user: User, avatar?: string): Promise<void> {
    const messageData: Message = {
      id: uuid(),
      text,
      userId: user.id,
      user: user.name,
      avatar: avatar || null,
      createdAt: Date.now(),
      editedAt: null,
      likedBy: []
    };
    dataStorageProvider.collection('messages').add(messageData);
  }

  public async updateMessage(id: string, message: Message): Promise<void> {
    const response: firestore.QuerySnapshot<firestore.DocumentData> = await dataStorageProvider.collection('messages')
      .where('id', '==', id)
      .limit(1)
      .get()
    if (response.size === 1) {
      const doc = response.docs[0];
      await doc.ref.update(message);
    }
  }

  public async deleteMessage(id: string): Promise<void> {
    const response: firestore.QuerySnapshot<firestore.DocumentData> = await dataStorageProvider.collection('messages')
      .where('id', '==', id)
      .limit(1)
      .get()
    if (response.size === 1) {
      const doc = response.docs[0];
      await doc.ref.delete();
    }
  }
}

export const messageService = new MessageService();
